import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure:false, // don't add this makes filter run everytime a change is detected
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString:string, propName:string): any {
  	if (value.length === 0 || filterString === '' || filterString === undefined) {
  		return value;
  	}
	const resultArray = [];
  	for (const item of value) {
  		if (item[propName]===filterString) {
  			resultArray.push(item);
  		}
  	}
	return resultArray;
  }

}
